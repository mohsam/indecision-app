import * as React from 'react';

interface Props {
    handlePick: VoidFunction;
    hasOptions: boolean;
}

const Action: React.FC<Props> = (props: Props) => (
    <div>
        <button
            className="big-button"
            onClick={props.handlePick}
            disabled={!props.hasOptions}
        >
            What should I do?
        </button>
    </div>
);
export default Action;
