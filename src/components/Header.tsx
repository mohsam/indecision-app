import * as React from 'react';

interface Props {
    subtitle?: string;
    title?: string;
}

const Header: React.FC<Props> = (props: Props) => (
    <div className="header">
        <div className="container">
            <h1 className="header__title">{props.title}</h1>
            {props.subtitle && (
                <h2 className="header__subtitle">{props.subtitle}</h2>
            )}
        </div>
    </div>
);
Header.defaultProps = {
    title: 'Indecision',
};
export default Header;
