import React from 'react';
import { shallow } from 'enzyme';
import Options from '../../components/Options';

test('should render empty Options', () => {
    const wrapper = shallow(
        <Options
            options={[]}
            handleDeleteOption={() => {}}
            handleDeleteOptions={() => {}}
        />
    );
    expect(wrapper).toMatchSnapshot();
});

test('should render Options', () => {
    const wrapper = shallow(
        <Options
            options={['option1', 'option2']}
            handleDeleteOption={() => {}}
            handleDeleteOptions={() => {}}
        />
    );
    expect(wrapper).toMatchSnapshot();
});
