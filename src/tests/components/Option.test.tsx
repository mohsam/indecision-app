import React from 'react';
import { shallow } from 'enzyme';
import Option from '../../components/Option';

test('should render Option correctly', () => {
    const wrapper = shallow(
        <Option optionText="text" count={1} handleDeleteOption={() => {}} />
    );
    expect(wrapper).toMatchSnapshot();
});

test('should render click on deleteOption', () => {
    let handleDeleteOption = jest.fn();
    const wrapper = shallow(
        <Option
            optionText="text"
            count={1}
            handleDeleteOption={handleDeleteOption}
        />
    );

    wrapper.find('.button--link').simulate('click');
    expect(handleDeleteOption).toHaveBeenCalled();
});
