import React from 'react';
import { shallow } from 'enzyme';
import OptionModal from '../../components/OptionModal';

test('should render OptionModal correctly', () => {
    const wrapper = shallow(
        <OptionModal
            selectedOption="text"
            handleClearSelectedOption={() => {}}
        />
    );
    expect(wrapper).toMatchSnapshot();
});
