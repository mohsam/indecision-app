## Overview

This project was implemented from [Udemy React Course](https://www.udemy.com/share/101XgIB0obcFdWR3Q=/)
It is an app where the user can enter his tasks then the app decides randomly what to todo next.

### Demo

[Click here](https://indecision-app-beta.herokuapp.com/) to view deployed version of the app.

## Install

`npm install`

### Start Development Server

`npm run dev`

### Start Production Server

`npm run start`

### Test

`npm run test`
`npm run test-coverage`

### Build

`npm run build`

### Start Docker container

`docker container run -p 3000:3000 indecision-app`

### Or with Docker Compose

`docker-compose up -d`

Open [http://localhost:3000](http://localhost:3000) to view development server in the browser.
Open [http://localhost:5000](http://localhost:5000) to view production server in the browser.
