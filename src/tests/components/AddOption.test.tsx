import React from 'react';
import { shallow } from 'enzyme';
import AddOption from '../../components/AddOption';

let wrapper: any, handleAddOtion: any;

beforeEach(() => {
    handleAddOtion = jest.fn();
    wrapper = shallow(<AddOption handleAddOtion={handleAddOtion} />);
});

test('should render AddOption correctly', () => {
    expect(wrapper).toMatchSnapshot();
});

test('should render AddOption with error', () => {
    wrapper
        .find('.add-option__input')
        .simulate('change', { target: { value: '' } });

    wrapper.find('.add-option').simulate('submit', {
        preventDefault: () => {},
        target: { elements: { option: { value: '' } } },
    });

    expect(handleAddOtion).toHaveBeenCalled();
});
