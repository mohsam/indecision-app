import React from 'react';
import { shallow } from 'enzyme';
import Action from '../../components/Action';

test('should render Action correctly', () => {
    const wrapper = shallow(<Action handlePick={() => {}} hasOptions={true} />);
    expect(wrapper).toMatchSnapshot();
});
