import React, { useState } from 'react';

interface Props {
    handleAddOtion: Function;
}

const AddOptions = (props: Props) => {
    const [error, setError] = useState('');
    const handleAddOtion = (e: any) => {
        e.preventDefault();
        const option: string = e.target.elements.option.value.trim();
        const error: string = props.handleAddOtion(option);
        setError(error);
        if (!error) {
            e.target.elements.option.value = '';
        }
    };

    return (
        <div>
            {error && <p className="add-option-error">{error}</p>}
            <form className="add-option" onSubmit={handleAddOtion}>
                <input
                    className="add-option__input"
                    type="text"
                    name="option"
                />
                <button className="button">Add option</button>
            </form>
        </div>
    );
};

export default AddOptions;
