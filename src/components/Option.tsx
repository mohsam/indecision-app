import * as React from 'react';

interface Props {
    optionText: string;
    count: number;
    handleDeleteOption: Function;
}

const Option: React.FC<Props> = (props: Props) => (
    <div className="option">
        <p className="option__text">
            {props.count}. {props.optionText}
        </p>
        <button
            className="button button--link"
            onClick={e => {
                props.handleDeleteOption(props.optionText);
            }}
        >
            remove
        </button>
    </div>
);
export default Option;
